
// 1.Які існують типи даних у Javascript?
// Примітивні типи:
// числа (number)
// рядки (string)
// булеві значення (boolean)
// null
// undefined
// символи (symbol)
// Об'єктні типи:
// об'єкти (object)
// масиви (array)
// функції (function)
// дати (date)
// інші, які визначаються розробником

// 2.У чому різниця між == і ===?
// В JavaScript оператор == перевіряє, чи два операнди мають однакове значення після
//  конвертації до одного типу даних. Оператор === перевіряє строго, чи два операнди мають 
//  однакове значення й тип даних.

// 3.Що таке оператор?
// Оператор - це символ або набір символів, який виконує певну операцію зі своїми операндами. 
// Наприклад, оператор + додає два числа разом, а оператор = присвоює значення зправа наліво.
//  В JavaScript існує багато операторів, включаючи арифметичні, логічні, порівняння та інші.


let userName;
let age;
do {
  userName = prompt("Whats your name?");
} while (!userName);

do {
  age = +prompt("How old are you?");
} while (isNaN(age));

switch (true) {
  case age < 18:
    alert(" You are not allowed to visit this website");
    break;
  case age >= 18 && age <= 22:
    let answer = confirm("Are you sure you want to continue?");
    if (answer === true) {
      alert(` Welcome ${userName} `);
    } else {
      alert(" You are not allowed to visit this website");
    }
    break;
  case age > 22:
    alert(` Welcome ${userName} `);
}
